package com.Entity;

/**
 * Created by VietAnh on 10/23/2016.
 */
public class FinancialData {
    private String oID;
    private String country;
    private int year;
    private String description;
    private String magnitude;
    private Double value;

    public FinancialData(String oID, String country, int year, String description, String magnitude, Double value) {
        this.oID = oID;
        this.country = country;
        this.year = year;
        this.description = description;
        this.magnitude = magnitude;
        this.value = value;
    }

    public String getoID() {

        return oID;
    }

    public void setoID(String oID) {
        this.oID = oID;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(String magnitude) {
        this.magnitude = magnitude;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return oID+"|"+country+"|"+year+"|"+description+"|"+magnitude+"|"+value;
    }
}
