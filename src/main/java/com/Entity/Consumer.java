package com.Entity;

import com.BO.CSVFileReader;

import java.util.concurrent.BlockingQueue;

/**
 * Created by VietAnh on 10/23/2016.
 */
public class Consumer implements Runnable {
    private BlockingQueue<FinancialData> queue;

    public Consumer(BlockingQueue<FinancialData> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while(true){
                FinancialData fd=queue.take();
                System.out.println("Taken new data from "+super.toString());
                Thread.sleep(2000);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}