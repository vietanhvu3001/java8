package com.Entity;

import com.BO.CSVFileReader;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Created by VietAnh on 10/23/2016.
 */
public class Producer implements Runnable {

    private BlockingQueue<FinancialData> queue = null;

    public Producer(BlockingQueue<FinancialData> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        CSVFileReader csf=new CSVFileReader();
        csf.read("UNdata_Export_20160419_033545375.csv");
        List<FinancialData>list=csf.getListData();
        try {
            System.out.println(list.size());
            for (FinancialData fd:list) {
                queue.put(fd);
                System.out.println("Putting new in queue: " + fd);
                Thread.sleep(1000);
            }
            System.out.println("Exit...!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
