package com.UI;

import com.BO.CSVFileReader;
import com.Entity.FinancialData;

import java.util.List;

/**
 * Created by VietAnh on 10/23/2016.
 */
public class MainJV8 {
    public static void main(String[] args) {
        CSVFileReader csvfr=new CSVFileReader();
//        String directtory =System.getProperty("user.dir");
//        System.out.println(directtory);
        csvfr.read("UNdata_Export_20160419_033545375.csv");
        List<FinancialData> list=csvfr.getListData();
        list.forEach(System.out::println);

    }
}
