package com.UI;

import com.BO.CSVFileReader;
import com.Entity.Consumer;
import com.Entity.FinancialData;
import com.Entity.Producer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by VietAnh on 10/23/2016.
 */
public class MainConcurency {
    public static void main(String[] args) {
//        CSVFileReader cfr=new CSVFileReader();
//        cfr.read("UNdata_Export_20160419_033545375.csv");
//        BlockingQueue<FinancialData> queue = cfr.getQueue();

        BlockingQueue<FinancialData> queue=new ArrayBlockingQueue<FinancialData>(100);
        Producer producer = new Producer(queue);
        Consumer c1 = new Consumer(queue);
        Consumer c2 = new Consumer(queue);
        Consumer c3 = new Consumer(queue);

        new Thread(producer).start();
        new Thread(c1, "Thread1").start();
        new Thread(c2, "Thread2").start();
        new Thread(c3, "Thread3").start();


    }
}

