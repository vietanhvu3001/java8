package com.BO;

import com.Entity.FinancialData;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class CSVFileReader {

    private List<FinancialData> listData = new ArrayList<FinancialData>();

    public List<FinancialData> getListData() {
        return listData;
    }

    public BlockingQueue<FinancialData> getQueue() {
        BlockingQueue<FinancialData> bq = new ArrayBlockingQueue<FinancialData>(listData.size());
        bq.addAll(getListData());
        return bq;
    }

    public void read(String fileName) {

        FileReader fileReader = null;
        CSVParser csvFileParser = null;
        String[] header = new String[]{"OID", "Country or Area", "Year", "Description", "Magnitude", "Value"};
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(header);

        try {
            fileReader = new FileReader(fileName);

            csvFileParser = new CSVParser(fileReader, csvFileFormat);

            List<CSVRecord> records = csvFileParser.getRecords();

            //Read the CSV file records starting from the second record to skip the header
            for (int i = 1; i < records.size(); i++) {
                CSVRecord record = records.get(i);
                //Create an object and set data
                FinancialData fd = new FinancialData(record.get("OID"), record.get("Country or Area"),
                        Integer.parseInt(record.get("Year")), record.get("Description"),
                        record.get("Magnitude"), Double.parseDouble(record.get("Value")));
                listData.add(fd);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
                if (csvFileParser != null) {
                    csvFileParser.close();
                }
            } catch (Exception ex) {
                System.out.println("Error closing");
            }
        }
    }

}
